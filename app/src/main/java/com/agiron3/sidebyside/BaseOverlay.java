package com.agiron3.sidebyside;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by andre on 2/28/15.
 */
public class BaseOverlay extends View{

    protected Paint paint; //Handles how to draw
    protected Context context;

    public BaseOverlay(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        init();
    }

    //Add necessary code to setup the paint object here.
    protected void init()
    {
        paint.setColor(Color.BLUE); //set its color to a default color (blue)
    }
}
