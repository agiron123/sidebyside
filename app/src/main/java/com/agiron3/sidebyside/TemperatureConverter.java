package com.agiron3.sidebyside;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class TemperatureConverter extends ActionBarActivity {

    private EditText celsiusEditText;
    private EditText farenheitEditText;
    private Button convertButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_converter);

        celsiusEditText = (EditText)findViewById(R.id.celciusEditText);
        farenheitEditText = (EditText)findViewById(R.id.farenheightEditText);
        convertButton = (Button)findViewById(R.id.convertButton);

        celsiusEditText.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                farenheitEditText.setText("*");
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        }); ;

        farenheitEditText.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                celsiusEditText.setText("*");
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(celsiusEditText.getText().toString().equals("*"))
                {
                    if(!farenheitEditText.getText().toString().equals("*"))
                    {
                        double tempF = Double.parseDouble(farenheitEditText.getText().toString());
                        double converted = (tempF - 32)*(5/9);
                        celsiusEditText.setText("" + converted);
                    }
                }
                else if(farenheitEditText.getText().toString().equals("*"))
                {
                    if(!celsiusEditText.getText().toString().equals("*"))
                    {
                        double tempC = Double.parseDouble(celsiusEditText.getText().toString());
                        double converted = tempC * (9/5) + 32;
                        farenheitEditText.setText("" + converted);
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_temperature_converter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
