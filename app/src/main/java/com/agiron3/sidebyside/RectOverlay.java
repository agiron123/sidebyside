package com.agiron3.sidebyside;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.Toast;

/**
 * Created by Andre on 4/14/2015.
 */
public class RectOverlay extends BaseOverlay
{
    private Rect drawRect;
    private Canvas canvas;
    private int color;

    public RectOverlay(Context context, AttributeSet attributeSet)
    {
        super(context, attributeSet);
    }

    public RectOverlay(Context context, Rect rect, int color)
    {
        this(context, (AttributeSet)null);
        this.drawRect = rect;
        this.color = color;

        this.setX(rect.left);
        this.setY(rect.top);

        this.setMinimumHeight(drawRect.height());
        this.setMinimumWidth(drawRect.width());
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        this.canvas = canvas;
        paint.setColor(this.color);
        canvas.drawRect(drawRect, paint);
    }

    public void setDrawRect(Rect drawRect)
    {
        super.setMinimumHeight(drawRect.height());
        super.setMinimumWidth(drawRect.width());
        this.drawRect = drawRect;
    }

    public Rect getDrawRect()
    {
        return drawRect;
    }

    public void setColor(int color)
    {
        this.color = color;
    }

}
