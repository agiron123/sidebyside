package com.agiron3.sidebyside;

import android.accessibilityservice.AccessibilityService;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;

/**
 * Created by Andre on 4/14/2015.
 */
public class SideBySideService extends AccessibilityService {

    private WindowManager windowManager;
    private int screenWidth, screenHeight;

    @Override
    public void onCreate()
    {
        windowManager = (WindowManager)getSystemService(WINDOW_SERVICE);

        screenWidth = this.getResources().getDisplayMetrics().widthPixels;
        screenHeight = this.getResources().getDisplayMetrics().heightPixels;

        //Create an overlay
        Rect redRect = new Rect();
        redRect.set(0,0,screenWidth,screenHeight/2);

        Rect blueRect = new Rect();
        blueRect.set(0,screenHeight/2, screenWidth, screenHeight/2);

        RectOverlay red = new RectOverlay(getBaseContext(), redRect, Color.RED);
        RectOverlay blue = new RectOverlay(getBaseContext(), blueRect, Color.BLUE);

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY | WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;

        params.x = 0;
        params.y = 0;
        params.width = screenWidth;
        params.height = screenHeight/2;

        //Add view to the overlay
        windowManager.addView(red, params);
        windowManager.addView(blue, params);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

    }

    @Override
    public void onInterrupt() {

    }
}
